const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT ? process.env.PORT : 56201;

app.listen(port);

const rawParser = bodyParser.raw({ type: 'text/plain' });

app.get('/', (req, res) => {
    res.send('Hello');
});

app.post('/square', rawParser, (req, res) => {
    if(JSON.stringify(req.body) === JSON.stringify({})){
        res.sendStatus(400);
    }
    else{
        const a = +req.body;
        res.send({
            number: a,
            square: a*a
        })
    }
})

app.post('/reverse', rawParser, (req, res) => {
    if(JSON.stringify(req.body) === JSON.stringify({})){
        res.sendStatus(400);
    }
    else{
        const raw = req.body.toString('utf-8');
        const reverse = raw.split('').reverse().join('');
        res.send(reverse)
    }
})

function leapYear(year){
    year = +year;
    return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

function checkDay(month, day, isLeep){
    if(month == '1' || month == '3' || month == '5' || month == '7' || month == '8' || month == '10' || month == '12'){
        return +day <= 31;
    }
    if(month == '4' || month == '6' || month == '9' || month == '11'){
        return +day <= 30;
    }
    if(month == '2'){
        if(isLeep)
            return +day <= 29;
        else
            return +day <= 28;
    }
}

app.get('/date/:year/:month/:day', rawParser, (req, res) => {
    const year = /^[1-9][0-9]{3}$/;
    const month = /^([1-9]|1[012])$/;
    const day = /^([1-9]|[12][0-9]|3[01])$/;

    const yearMatch = req.params['year'].match(year);
    const monthMatch = req.params['month'].match(month);
    const dayMatch = req.params['day'].match(day);
    console.log(yearMatch)
    console.log(monthMatch)
    console.log(dayMatch)
    console.log(checkDay(req.params['month'], req.params['day'], leapYear(req.params['year'])))
    if (yearMatch == null || monthMatch == null || dayMatch == null || !checkDay(req.params['month'], req.params['day'], leapYear(req.params['year']))){
        res.sendStatus(400);
    } else{
        const dateStr = `${req.params['year']}-${req.params['month']}-${req.params['day']}`
        const date = new Date(dateStr);
        const todayFull = new Date();
        
        const cYear = todayFull.getFullYear();
        const cMonth = todayFull.getMonth() + 1 < 10 ? `0${todayFull.getMonth() + 1}` : todayFull.getMonth() + 1;
        const cDay = todayFull.getDate() < 10? `0${todayFull.getDate()}` : todayFull.getDate();
        const cDateStr = `${cYear}-${cMonth}-${cDay}`
        const today = new Date(cDateStr)
        const differenceTime = Math.abs(date-today);
        const differenceDay = Math.ceil((differenceTime) / (1000 * 60 * 60 * 24));
        const response = {
            weekDay: date.toLocaleDateString('default', { weekday: 'long' }),
            isLeapYear: leapYear(req.params['year']),
            difference: differenceDay
        };
        res.send(response);
    }
})
